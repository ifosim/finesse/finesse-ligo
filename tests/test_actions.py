import pytest

from finesse.ligo.factory import aligo
import finesse.ligo.actions as flac

from .test_factory import PARAMETER_FILES


@pytest.mark.parametrize("file", PARAMETER_FILES)
@pytest.mark.parametrize("state", flac.DRFPMI_state.states)
def test_DRFPMI_state(file, state):
    """Check each of the states works with both aLIGO models."""
    factory = aligo.ALIGOFactory(file)

    model = factory.make()
    model.run(flac.DRFPMI_state(state))
