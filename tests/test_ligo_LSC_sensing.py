# %%
import numpy as np
import matplotlib.pyplot as plt
import scipy.constants as scc
from scipy.special import jv
import pytest

import finesse.analysis.actions as fa
import finesse.components as fc
import finesse.detectors as fd
from finesse.symbols import Constant
from finesse_ligo.factory import aligo
from munch import Munch
from finesse.plotting import bode

######################################################################
# Define some plotting helpers
######################################################################


def makegrid(ax, xarr=None):
    ax.grid(True, which="major", alpha=0.5)
    ax.grid(True, which="minor", alpha=0.2)
    if xarr is not None:
        ax.set_xlim(xarr[0], xarr[-1])


def set_yticks(ax):
    ymin, ymax = ax.get_ylim()
    powers = np.arange(np.floor(np.log10(ymin)), np.ceil(np.log10(ymax)) + 1)
    ticks = np.array([10**power for power in powers])
    ax.yaxis.set_ticks(ticks)


def plot_tf(f, tf, axs=None, figsize=(6, 6), **kwargs):
    """Spruce up the finesse bode function to plot transfer functions."""
    if axs is None:
        fig = plt.figure(figsize=figsize)
        gs = fig.add_gridspec(2, 1, height_ratios=[1, 1], hspace=0.05)
        axs = [fig.add_subplot(gs[0])]
        axs.append(fig.add_subplot(gs[1], sharex=axs[0]))
    bode(f, tf, axs=axs, db=False, **kwargs)
    ylim = axs[0].get_ylim()
    if ylim[1] / ylim[0] < 10:
        axs[0].set_ylim(ylim[0] / 10.1, ylim[1] * 10.1)
    axs[0].set_xlim(f[0], f[-1])
    axs[1].set_ylim(-185, 185)
    ticks = np.arange(-180, 181, 45)
    axs[1].yaxis.set_ticks(ticks)
    axs[0].xaxis.set_tick_params(labelbottom=False)
    for ax in axs:
        ax.grid(True, which="major", alpha=0.5)
        ax.grid(True, which="minor", alpha=0.2)
        leg = ax.get_legend()
        if leg is not None:
            leg.set_visible(False)
    set_yticks(axs[0])
    return axs


def plot_tf12(f, tf1, tf2, fig=None, figsize=(10, 6), **kwargs):
    """Plot two side by side transfer functions.

    Useful for plotting I and Q quadratures next to each other.
    """
    if fig is None:
        fig = plt.figure(figsize=figsize)
        gs = fig.add_gridspec(1, 2, wspace=0.25)
        gs1 = gs[0, 0].subgridspec(2, 1, hspace=0.05)
        gs2 = gs[0, 1].subgridspec(2, 1, hspace=0.05)
        axs1 = [fig.add_subplot(gs1[0])]
        axs1.append(fig.add_subplot(gs1[1], sharex=axs1[0]))
        axs2 = [fig.add_subplot(gs2[0])]
        axs2.append(fig.add_subplot(gs2[1], sharex=axs2[0]))
    else:
        axs1 = fig.axes[:2]
        axs2 = fig.axes[2:]
    plot_tf(f, tf1, axs1, **kwargs)
    plot_tf(f, tf2, axs2, **kwargs)
    return fig


######################################################################
# Define aLIGO model and helper functions
######################################################################


class ALIGOFactory(aligo.ALIGOFactory):
    """Tweak ALIGOFactory for ease of comparison and debugging."""

    def set_default_options(self):
        super().set_default_options()
        self.options.LSC.add_output_detectors = True

    def set_default_LSC(self):
        super().set_default_LSC()
        self.LSC_output_matrix.MICH = Munch()
        self.LSC_output_matrix.MICH2 = Munch()
        self.LSC_output_matrix.MICH3 = Munch()
        self.LSC_output_matrix.MICH.BS = +np.sqrt(2)
        self.LSC_output_matrix.MICH2.BS = +np.sqrt(2)
        self.LSC_output_matrix.MICH2.PRM = -1
        self.LSC_output_matrix.MICH2.SRM = +1
        self.LSC_output_matrix.MICH3.ITMX = +1
        self.LSC_output_matrix.MICH3.ETMX = -1
        self.LSC_output_matrix.MICH3.ITMY = -1
        self.LSC_output_matrix.MICH3.ETMY = +1

        # self.LSC_output_matrix.MICH2.ITMX = +1
        # self.LSC_output_matrix.MICH2.ETMX = -1
        # self.LSC_output_matrix.MICH2.ITMY = -1
        # self.LSC_output_matrix.MICH2.ETMY = +1

    def add_LSC_readouts(self, model):
        super().add_LSC_readouts(model)
        f1 = model.f1.ref
        output_detectors = self.options.LSC.add_output_detectors
        model.add(
            fc.ReadoutRF("AS9", self.AS_port.o, f=f1, output_detectors=output_detectors)
        )

    def add_detectors(self, model):
        super().add_detectors(model)

        def add_power_detectors(key):
            Pu = np.abs(Constant(model.get(f"a_u{key}"))) ** 2
            Pl = np.abs(Constant(model.get(f"a_l{key}"))) ** 2
            model.add(fd.MathDetector(f"P_u{key}", Pu))
            model.add(fd.MathDetector(f"P_l{key}", Pl))

        add_power_detectors("9_prc")
        add_power_detectors("45_prc")
        add_power_detectors("9_src")
        add_power_detectors("45_src")


# DC_offset_pm = lambda p: fa.PrintModelAttr(
#     ("DARM.DC", lambda x: x * 1064e-9 / 360 * 1e12), prefix=p
# )
DC_offset_pm = lambda p: fa.PrintModelAttr("DARM.DC", prefix=p)


def get_params(model):
    """Calculate parameters from analytic study of DRFPMI frequency response Part 1:

    https://dcc.ligo.org/LIGO-T1500325 (No DC offset for DARM RF readout) Part 2:
    https://dcc.ligo.org/LIGO-T1500461 (DC offset for DARM DC readout)
    """
    # lengths, Eq 1
    L_D = (model.LX.L - model.LY.L) / 2  # DARM
    L_C = (model.LX.L + model.LY.L) / 2  # CARM
    l_p = model.L_PRC.eval()
    l_s = model.L_SRC.eval()
    l_x = model.l_x.eval()
    l_y = model.l_y.eval()
    l_m = (l_x - l_y) / 2  # MICH
    l_sch = l_m  # Schnupp

    # modulation
    w1 = 2 * np.pi * model.f1.eval()
    w2 = 2 * np.pi * model.f2.eval()
    gamma1 = model.mod1.midx.value
    gamma2 = model.mod2.midx.value

    # reflectivities
    re = np.sqrt(model.ETMX.R.eval())
    ti = np.sqrt(model.ITMX.T.eval())
    ri = np.sqrt(model.ITMX.R.eval())
    ts = np.sqrt(model.SRM.T.eval())
    rs = np.sqrt(model.SRM.R.eval())
    rp = np.sqrt(model.PRM.R.eval())
    tp = np.sqrt(model.PRM.T.eval())

    # pole frequencies
    # Eq 12
    w_rse = (
        scc.c
        / (2 * L_C)
        * np.log((1 - ri * rs) / (re * ri - re * rs * (ti**2 + ri**2)))
    )
    f_rse = w_rse / (2 * np.pi)
    # Eq 15
    w_cc = (
        scc.c
        / (2 * L_C)
        * np.log((1 + ri * rp) / (re * ri + re * rp * (ti**2 + ri**2)))
    )
    f_cc = w_cc / (2 * np.pi)

    c = Munch()
    c.ra = (re * (ti**2 + ri**2) - ri) / (1 - ri * re)  # Eq 2
    c.ra_dp = ti**2 * re / (1 - ri * re) ** 2  # Eq 3
    c.rc = ((rp**2 + tp**2) * c.ra - rp) / (1 - rp * c.ra)  # Eq 5

    # Eq 6
    gp = tp / (1 - rp * c.ra)
    gs = ts / (1 + rs * c.ra)

    # DARM offset, II Eq 3 (eps_rad = 2 * pi * dL / lambda0)
    eps = model.DARM.DC

    def make_sidebands(wm, gamma, freq):
        """Calculate sideband dependent quantities."""
        assert freq in [9, 45]
        sb = Munch()
        sb.ra = -(re * (ti**2 + ri**2) + ri) / (1 + ri * re)  # Eq 2
        sb.ra_dp = ti**2 * re / (1 + ri * re) ** 2  # Eq 3

        # Michelson reflectivity and transmisision, Eq 4
        if freq == 9:
            sb.rM = sb.ra * np.cos(2 * w1 * l_sch / scc.c)
            sb.tM = sb.ra * np.sin(2 * w1 * l_sch / scc.c)
            sb.r_sm = (sb.rM - sb.ra**2 * rs) / (1 - rs * sb.rM)
            sb.t_sm = ts * sb.tM / (1 - rs * sb.rM)

        elif freq == 45:
            sb.rM = sb.ra * np.cos(2 * w2 * l_sch / scc.c)
            sb.tM = sb.ra * np.sin(2 * w2 * l_sch / scc.c)
            sb.r_sm = (sb.rM + sb.ra**2 * rs) / (1 + rs * sb.rM)
            sb.t_sm = ts * sb.tM / (1 + rs * sb.rM)
            # sb.r_sb = - ((rp**2 + tp**2) * sb.r_sm + rp) / (1 + rp * sb.r_sm)

        # Eq 5
        sb.r_sb = -((rp**2 + tp**2) * sb.r_sm + rp) / (1 + rp * sb.r_sm)
        # Eq 6
        sb.g_sb = tp / (1 + rp * sb.r_sm)

        S0 = 2 * jv(0, gamma) * jv(1, gamma)  # normalization factor
        # PRCL REFL zero, Eq 16
        sb.w_r = w_cc * (1 + gp**2 * c.ra * sb.r_sb / (sb.g_sb**2 * c.rc * sb.r_sm))
        sb.f_r = sb.w_r / (2 * np.pi)
        # PRCL POP zero, Eq 18
        sb.w_p = w_cc * (1 - gp / sb.g_sb)
        sb.f_p = sb.w_p / (2 * np.pi)
        # SRCL REFL zero, II Eq 15 and 16
        sb.tc = 2 * gp * gs * c.ra_dp * eps
        sb.w_sr = w_cc * (1 - sb.tc**2 * sb.r_sb / (sb.g_sb**2 * c.rc * sb.t_sm**2))
        sb.f_sr = sb.w_sr / (2 * np.pi)
        # SRCL POP zero, III Eq 17
        sb.w_sp = w_cc * (1 + sb.r_sm * sb.tc**2 / (gp * sb.g_sb * c.ra * sb.t_sm**2))
        sb.f_sp = sb.w_sp / (2 * np.pi)

        return sb

    sb9 = make_sidebands(w1, gamma1, 9)
    sb45 = make_sidebands(w2, gamma2, 45)

    return Munch(locals())


######################################################################
# Analyze LSC frequency response and error signals
######################################################################


@pytest.mark.example
@pytest.mark.parametrize("DC_readout", [True, False])
def test_fresp_err_sigs(DC_readout, tpath_join):
    """Frequency response plots should be compared with figures 3 and 4 of
    https://dcc.ligo.org/LIGO-T1500325 and figures 2 and 3 of part 2 of
    https://dcc.ligo.org/LIGO-T1500461."""
    if DC_readout:
        post_RF_lock_action = aligo.DARM_RF_to_DC()
        plt_title = "DARM DC Readout"
    else:
        post_RF_lock_action = fa.Change({"DARM.DC": 0})
        plt_title = "DARM RF Readout"
    F_Hz = np.geomspace(1e-2, 1e5, 500)
    factory = ALIGOFactory("lho_O4.yaml")
    model = factory.make()
    model.fsig.f = 1
    model.modes("off")
    dofs = ["DARM", "CARM", "PRCL", "SRCL", "MICH", "MICH2"]
    detectors = [f"{det}{freq}" for det in ["REFL", "POP", "AS"] for freq in [9, 45]]
    print(30 * "-")
    sol = model.run(
        fa.Series(
            aligo.InitialLock(),
            DC_offset_pm("post RF lock"),
            post_RF_lock_action,
            DC_offset_pm("post DC lock"),
            fa.FrequencyResponse(
                F_Hz,
                [f"{dof}.AC" for dof in dofs],
                [f"{det}.{phase}" for det in detectors for phase in ["I", "Q"]]
                + ["AS.DC"],
                open_loop=True,
                name="fresp",
            ),
            fa.SensingMatrixDC(
                dofs,
                detectors,
                name="sensing",
            ),
            fa.Xaxis(model.PRCL.DC, "lin", -2.5, 2.5, 250, relative=True, name="PRCL"),
            fa.Xaxis(model.SRCL.DC, "lin", -15, 15, 250, relative=True, name="SRCL"),
            fa.Xaxis(model.DARM.DC, "lin", -1.5, 1.5, 250, relative=True, name="DARM"),
            fa.Xaxis(
                model.CARM.DC, "lin", -0.15, 0.15, 250, relative=True, name="CARM"
            ),
            fa.Xaxis(model.MICH.DC, "lin", -5, 5, 250, relative=True, name="MICH"),
            fa.Xaxis(model.MICH2.DC, "lin", -5, 5, 250, relative=True, name="MICH2"),
            fa.Xaxis(model.lp3.L, "lin", -1, 1, 250, relative=True, name="PRCL_scan"),
            fa.Xaxis(
                model.ls3.L, "lin", -2.5, 2.5, 250, relative=True, name="SRCL_scan"
            ),
        )
    )

    ##################################################
    # Frequency Response
    ##################################################

    tfs = sol["fresp"]
    params = get_params(model)
    print(30 * "-")
    print("DC offset", params.eps * 1064e-9 / 360 * 1e12)

    # DOF styles for plots
    dofstyles = Munch(
        DARM=dict(c="xkcd:cerulean", label="DARM"),
        CARM=dict(c="xkcd:orange", label="CARM"),
        PRCL=dict(c="xkcd:royal blue", label="PRCL"),
        SRCL=dict(c="xkcd:pinkish purple", label="SRCL"),
        MICH=dict(c="xkcd:red", label="MICH"),
        MICH2=dict(c="xkcd:kelly green", label="MICH2", ls="--"),
    )

    # styles for marking special frequencies
    fstyles = Munch(
        f_cc=dict(ls="--", c="xkcd:cerulean", label=r"$f_\mathrm{cc}$"),
        f_rse=dict(ls="--", c="xkcd:dark orange", label=r"$f_\mathrm{rse}$"),
        f_p=dict(ls=":", c="xkcd:scarlet", label=r"$f_\mathrm{p}$"),
        f_r=dict(ls=":", c="xkcd:scarlet", label=r"$f_\mathrm{r}$"),
        f_sp=dict(ls=":", c="xkcd:green", label=r"$f_\mathrm{sp}$"),
        f_sr=dict(ls=":", c="xkcd:green", label=r"$f_\mathrm{sr}$"),
    )

    def plot_readout(detector, dtype):
        """Make a frequency response plot for a given detector."""
        assert dtype in ["RF", "DC"]

        if dtype == "RF":
            tfIQ = lambda x: (
                tfs[f"{x}.AC", f"{detector}.I"],
                tfs[f"{x}.AC", f"{detector}.Q"],
            )

            def plot_func(dof, ret=None, **kwargs):
                kwargs.update({"fig": ret})
                return plot_tf12(F_Hz, *tfIQ(dof), **kwargs)

        elif dtype == "DC":

            def plot_func(dof, ret=None, **kwargs):
                kwargs.update({"axs": ret})
                return plot_tf(F_Hz, tfs[f"{dof}.AC", f"{detector}.DC"], **kwargs)

        ret = plot_func("DARM", **dofstyles.DARM)
        plot_func("CARM", ret=ret, **dofstyles.CARM)
        plot_func("PRCL", ret=ret, **dofstyles.PRCL)
        plot_func("SRCL", ret=ret, **dofstyles.SRCL)
        plot_func("MICH", ret=ret, **dofstyles.MICH)
        plot_func("MICH2", ret=ret, **dofstyles.MICH2)
        if dtype == "RF":
            fig = ret
            for ax in [fig.axes[0], fig.axes[2]]:
                ax.legend(ncol=2, fontsize="small")
            fig.axes[0].set_title(f"{plt_title} {detector} I")
            fig.axes[2].set_title(f"{plt_title} {detector} Q")
        elif dtype == "DC":
            ret[0].legend(ncol=2, fontsize="small")
            ret[0].set_title(f"{plt_title} {detector} DC")
            fig = ret[0].figure
        return fig

    def add_frequency_lines(fig, freq_list):
        """Mark special frequencies."""
        for idx, ax in enumerate(fig.axes):
            handles = []
            labels = []
            for freq, style in freq_list:
                handles.append(ax.axvline(np.abs(freq), **style))
                labels.append(style["label"])
                # labels.append(f"{style['label']}={freq:0.1f} Hz")
            if idx % 2 == 1:
                ax.legend(handles, labels, fontsize="large")

    # print specific frequencies
    print(30 * "-")
    print("PRCL")
    print("REFL9 f_r", params.sb9.f_r)
    print("REFL45 f_r", params.sb45.f_r)
    print("POP9 f_p", params.sb9.f_p)
    print("POP45 f_p", params.sb45.f_p)
    print(30 * "-")
    print("SRCL")
    print("REFL9 f_sr", params.sb9.f_sr)
    print("REFL45 f_sr", params.sb45.f_sr)
    print("POP9 f_sp", params.sb9.f_sp)
    print("POP45 f_sp", params.sb45.f_sp)
    print(30 * "-")
    print("f_cc", params.f_cc)
    print("f_rse", params.f_rse)
    print(30 * "-")

    # actually make all the frequency response plots
    for det in ["REFL", "POP", "AS"]:
        for freq in [9, 45]:
            freqs = []
            if det in ["REFL", "POP"]:
                freqs.append((params.f_cc, fstyles.f_cc))
            if det == "AS":
                freqs.append((params.f_rse, fstyles.f_rse))
            if det == "REFL":
                freqs.append((params[f"sb{freq}"].f_r, fstyles.f_r))
                if DC_readout:
                    freqs.append((params[f"sb{freq}"].f_sr, fstyles.f_sr))
            if det == "POP":
                freqs.append((params[f"sb{freq}"].f_p, fstyles.f_p))
                if DC_readout:
                    freqs.append((params[f"sb{freq}"].f_sp, fstyles.f_sp))
            fig = plot_readout(f"{det}{freq}", "RF")
            add_frequency_lines(fig, freqs)
            fig.savefig(tpath_join(f"{det}{freq}.pdf"))
    fig = plot_readout("AS", "DC")
    add_frequency_lines(fig, [(params.f_rse, fstyles.f_rse)])
    fig.savefig(tpath_join("AS_DC.pdf"))

    ##################################################
    # PDH error signals
    ##################################################

    def plot_error_signal(dof, readout):
        fig, ax = plt.subplots()
        x_deg = sol[dof].x[0]
        ax.plot(x_deg, sol[dof, f"{readout}_I"], label=f"{readout} I")
        ax.plot(x_deg, sol[dof, f"{readout}_Q"], label=f"{readout} Q")
        ax.legend()
        makegrid(ax, x_deg)
        ax.set_xlabel(f"Relative {dof} tuning [deg]")
        ax.set_ylabel("Power [W]")
        ax.set_title(f"{plt_title} {dof} PDH Error Signal")
        fig.savefig(tpath_join(f"{dof}_PDH.pdf"))

    plot_error_signal("PRCL", "POP9")
    plot_error_signal("SRCL", "POP45")
    plot_error_signal("DARM", "AS45")
    plot_error_signal("CARM", "REFL9")
    plot_error_signal("MICH", "POP45")
    plot_error_signal("MICH2", "POP45")

    ##################################################
    # PRC/SRC scans
    ##################################################

    def plot_sweep(dof, sb_key):
        fig, ax = plt.subplots()
        x_m = sol[f"{dof}_scan"].x[0]
        ax.semilogy(x_m, sol[f"{dof}_scan", f"P_u9_{sb_key}"], label="+9 MHz")
        ax.semilogy(x_m, sol[f"{dof}_scan", f"P_l9_{sb_key}"], label="-9 MHz", ls="--")
        ax.semilogy(x_m, sol[f"{dof}_scan", f"P_u45_{sb_key}"], label="+45 MHz")
        ax.semilogy(
            x_m, sol[f"{dof}_scan", f"P_l45_{sb_key}"], label="-45 MHz", ls="--"
        )
        ax.legend()
        makegrid(ax, x_m)
        ax.set_xlabel(f"Change in {sb_key.upper()} length from nominal [m]")
        ax.set_ylabel(f"Power in {sb_key.upper()} [W]")
        ax.set_title(f"{plt_title} {dof} Sweep")
        fig.savefig(tpath_join(f"{dof}_sweep.pdf"))

    plot_sweep("PRCL", "prc")
    plot_sweep("SRCL", "src")

    ##################################################
    # sensing matrix
    ##################################################

    fig, _ = sol["sensing"].plot(2, 3)
    fig.savefig(tpath_join("sensing.pdf"))


@pytest.mark.example
def test_MICH(tpath_join):
    F_Hz = np.geomspace(1e-2, 1e5, 500)
    factory = ALIGOFactory("lho_O4.yaml")
    factory.options.BS_type = "thick"
    model = factory.make()
    model.fsig.f = 1
    model.modes("off")
    detectors = [f"{det}{freq}" for det in ["REFL", "POP", "AS"] for freq in [9, 45]]
    sol = model.run(
        fa.Series(
            aligo.InitialLock(),
            aligo.DARM_RF_to_DC(),
            DC_offset_pm("post DC lock"),
            fa.PrintModelAttr("MICH.DC"),
            # fa.Change({"MICH2.DC": -45}, relative=True),
            fa.FrequencyResponse(
                F_Hz,
                ["MICH.AC", "MICH2.AC", "MICH3.AC", "DARM.AC"],
                # ["MICH.AC", "MICH2.AC", "MICH3.AC"],
                [f"{det}.{phase}" for det in detectors for phase in ["I", "Q"]]
                + ["AS.DC"],
                open_loop=True,
                name="fresp",
            ),
            fa.Noxaxis(name="DC"),
        ),
    )

    params = get_params(model)
    tfs = sol["fresp"]
    arm_gain = (sol["DC", "AGX"] + sol["DC", "AGY"]) / 2
    print(arm_gain)

    axs = plot_tf(F_Hz, tfs["MICH.AC", "AS.DC"], label="MICH")
    plot_tf(F_Hz, tfs["MICH2.AC", "AS.DC"], axs=axs, label="MICH2", ls="--")
    # plot_tf(F_Hz, tfs["MICH3.AC", "AS.DC"], axs=axs, label="MICH3", ls=':')
    plot_tf(
        F_Hz,
        tfs["DARM.AC", "AS.DC"] / arm_gain,
        axs=axs,
        label="DARM/(Arm Gain)",
        ls=":",
    )
    axs[0].legend()
    for ax in axs:
        ax.axvline(params.f_rse, ls=":", c="xkcd:scarlet")
    axs[0].figure.savefig(tpath_join("AS_DC.pdf"))
