.. include:: /defs.hrst

.. _intro:

Introduction to Finesse LIGO
============================

|Finesse| LIGO is a package that extends the core |Finesse| tool. For general
information on how to use |Finesse| please visit `<http://finesse.ifosim.org>`_.
This package contains various features that extends |Finesse| allowing you to
model the LIGO detectors

* Customisable LIGO models that can be specialised to either LIGO Hanford or Livingston
* Additional LIGO specific elements like quadrupule and triple suspension models
* Additional |Finesse| actions to perform common routines
* Functions for downloading and manipulating LIGO specific data files
  * Maps
  * Suspension models
  * Apertures

The main repository for this LIGO package is stored on a publically readable but
write restricted Gitlab instance. This is hosted by the LIGO Scientific
Collaboration at this `git repository <https://git.ligo.org/finesse/finesse-ligo>`_.
