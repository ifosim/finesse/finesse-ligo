.. include:: /defs.hrst
.. _examples_srcl_dither:

Arm power estimation using SRCL dither
--------------------------------------

It is possible to make an estimate of the arm power within LIGO by dithering
SRCL and infering the induced change in the DARM length as well as the power
coupling to the arm transmission photodiodes. See 3.2 in :cite:`phdCraig` for
more details on the math.

This is an exmaple of how to use a LIGO model, compute and maniuplate some
transfer function measurements to replicate what might be done at the
instrument.

First we setup the Livingston model and add some extra transmission readouts
that we will need. We also suspend the SRM using a simple free mass model which
should be valid for frequencies higher than 10 Hz.

.. jupyter-execute::

    import matplotlib.pyplot as plt
    import finesse
    import finesse.analysis.actions as fac
    import finesse.components
    import finesse_ligo
    import numpy as np
    from scipy.optimize import curve_fit
    from finesse_ligo.factory import ALIGOFactory
    from finesse_ligo.actions import InitialLockLIGO, DARM_RF_to_DC

    finesse.init_plotting()

    # %% Load in the parameter file
    factory = ALIGOFactory("llo_O4.yaml")

    # Can also use a free mass here if you want
    # from finesse.components.mechanical import FreeMass
    # mass = 40
    # factory.options.QUAD_suspension_model = FreeMass
    # factory.options.QUAD_suspension_kwargs = {"mass": mass}

    factory.options.QUAD_suspension_model = finesse_ligo.suspension.QUADSuspension
    mass = 40  # mass is baked into the QUAD state-space model
    llo = factory.make()

    llo.parse(
        """
        readout_dc TRX ETMX.p2.o output_detectors=True
        readout_dc TRY ETMY.p2.o output_detectors=True
        free_mass SRM_sus SRM.mech mass=1
    """
    )

Then we lock and change the model to use DC readout rather than RF for the DARM.
This has some subtle changes to the transfer functions at lower frequencies, but
the arm power inference works regardless of which is used here. By default the
model is setup using RF for DARM.

.. jupyter-execute::

    llo.modes("even", maxtem=6)
    lock = InitialLockLIGO(
        exception_on_lock_fail=False,
        lock_steps=100,
        gain_scale=0.4,
        pseudo_lock_arms=False,
        run_locks=True,
    )
    sol = llo.run(lock)
    llo.run(DARM_RF_to_DC())

Now we run the measurements using the frequency response action to compute a
force on the SRM to the various outputs we need. We also get the DC fields
everywhere just to calculate powers. You could also use power detectors and run
a :external:class:`~finesse.analysis.actions.axes.Noxaxis`.

Here we define a function to do the SRCL dither measurement and calculations.
For this analysis we need to fit a :math:`1/f^2` and :math:`1/f^4` slope to the
higher frequency parts of the transfer functions, above 10Hz works, but the
result will be sensitive to how well this fit is done.

.. jupyter-execute::

    def SRCL_dither(model, f, f_min):
        sol = model.run(
            fac.Series(
                # Measure transfer functions from SRCL control signal to
                # arm transmission and the induced DARM motions
                fac.FrequencyResponse(
                    f,
                    "SRM.mech.F_z",
                    [
                        "TRX.DC",
                        "TRY.DC",
                        "DARM.AC.o",
                    ],
                    name="fresp",
                ),
                fac.DCFields(name="dc"),
            )
        )
        dc = sol["dc"]
        fresp = sol["fresp"]

        P_TRX = np.sum(abs(dc["ETMX.p2.o"]) ** 2, -1).squeeze()[0]
        P_TRY = np.sum(abs(dc["ETMY.p2.o"]) ** 2, -1).squeeze()[0]

        # Normalise into RIN by dividing by DC power
        TRX_SRCL = fresp["TRX.DC", "SRM.mech.F_z"] / P_TRX
        TRY_SRCL = fresp["TRY.DC", "SRM.mech.F_z"] / P_TRY
        DARM_SRCL = fresp["DARM.AC.o", "SRM.mech.F_z"]

        f_slice = f >= f_min
        _f = f[f_slice]

        popt, _ = curve_fit(lambda x, a: a / x**4, _f, abs(DARM_SRCL)[f_slice])
        alpha_1 = popt[0]

        popt, _ = curve_fit(lambda x, a: a / x**2, _f, abs(TRX_SRCL)[f_slice])
        alpha_2 = popt[0]

        P_arm_est = alpha_1 / alpha_2 * np.pi**2 * mass * finesse.constants.C_LIGHT
        plt.figure()
        plt.loglog(fresp.f, abs(TRX_SRCL), label="TRX/SRCL [W/N]")
        plt.loglog(fresp.f, abs(TRY_SRCL), label="TRY/SRCL [W/N]")
        plt.loglog(fresp.f, abs(DARM_SRCL), label="DARM/SRCL [L/N]")
        plt.ylim(plt.ylim())
        plt.loglog(fresp.f, alpha_1 / fresp.f**4, ls="--", label=r"$\alpha_1 / f^{4}$")
        plt.loglog(fresp.f, alpha_2 / fresp.f**2, ls="--", label=r"$\alpha_2 / f^{2}$")
        plt.loglog()
        plt.legend()
        plt.xlabel("Frequency [Hz]")
        plt.ylabel("Amplitude")

        P_arm_act = (abs(dc["ETMX.p1.i"][0, 0]) ** 2).sum()

        plt.title(
            f"Estimated $P_{{arm}} = {P_arm_est/1e3:.3f}$ kW\nActual $P_{{arm}} = {P_arm_act/1e3:.3f}$ kW"
        )

        return P_arm_est, P_arm_act

Now we can run the current model and see how the power estimation works:

.. jupyter-execute::

    f = np.geomspace(0.1, 100, 101)

    with llo.temporary_parameters():
        llo.run("run_locks()")
        P_arm_est, P_arm_act = SRCL_dither(llo, f, 10)

    assert abs((P_arm_est - P_arm_act) / P_arm_act) < 0.05  # ensure better than 5%

When everything is perfect the above analysis works quite well. However, if we
add some imperfections between the arms (say from thermal lensing) and
differences between the ITM transmissions (as the LHO detector in O4 is) we find
the SRCL measurement can under-predict the arm power. We first apply some
offsets in the model parameters and the re-lock to get back to a good operating
point. Note that if you make these changes to extreme the locking process won't
work.

From some experimenting it seems that the fitting of the TR/SRCL transfer
functions are the most sensitive, in particular to differential test curvatures
errors. Which are no doubt likely in the real interferometer.

.. jupyter-execute::

    with llo.temporary_parameters():
        llo.ETMX.Rc = 2260
        llo.ETMY.Rc = 2235
        llo.run("run_locks()")

        SRCL_dither(llo, f, 10)
        SRCL_dither(llo, f, 50)

We should see that this power estimation isn't quite as accurate. A simple
solution here is to increase what frequency we start fitting the polynomials at.
Clearly the lower frequency parts are becoming distorted and not well
approximated by the simple :math:`1/f^2` and :math:`1/f^4` slopes. Of course,
some of this can be solved by just fitting to higher frequency points of the
transfer function.
