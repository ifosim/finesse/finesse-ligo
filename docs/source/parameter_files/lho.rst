.. include:: /defs.hrst
.. _parameter_file_lho:

Hanford O4
==========

Select this parameter file using :code:`lho_O4.yaml` when making the factory.

.. literalinclude:: ../../../src/finesse_ligo/parameter_files/lho_O4.yaml
   :language: yaml
   :linenos:
