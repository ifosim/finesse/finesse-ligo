.. include:: /defs.hrst
.. _basics:

Introduction
============

Modelling in |Finesse| is a three step process: first you make a model, then run
simulations on that model, then analyse the results.

:external:class:`Models <finesse.model.Model>` are descriptions of an interferometric layout,
they contain the optical elements, how they are connected, simulated detectors,
readouts for measuing error signals, and descriptions of how the interferometer
should be controlled. A :external:class:`Model <finesse.model.Model>` is
therefore a high-level description of an experiment.

Once a model has been made to your requirements you can then run a simulation
with it. For example, varying some interferometer parameter and see how the
model behaves.

Reading material
----------------

The LIGO model is about as complicated as one might run in |Finesse|. This
documentation has to assume some basic level knowledge of LIGO, its components,
and how it works. Teaching everything is beyond what is possible.

If you do not know anything about LIGO and how it works we suggest you read the
following, all of it pretty much:

* Living Review :cite:`LivingReview`
* The |Finesse| 3 manual `<https://finesse.ifosim.org/docs/latest/>`_. The
  chances of the LIGO models making any sense without understanding what the
  core of |Finesse| does is slim.

This may seem like a lot, however getting accurate results from these LIGO
models requires an appreciation of the instrument and the mathematics being solved.

Quickstart
----------

The full model contains many, many optical components describing everything from the
input mode cleaners to the output mode cleaner, DC and AC control loops, thermal
deformations, apertures, suspensions. The art of modelling requires knowing what
to use when and keeping it as simple as you can. Unforunately, keeping it simple
is not always possible as LIGO is a complex machine and will need to combine
physical effects.

This selectivity of features is where the :class:`finesse_ligo.factory.aligo.ALIGOFactory` pattern comes in. A factory
is a higher-level concept than a standard |Finesse| model: a factory produces a
LIGO model that meets a certain specification and feature set.

.. jupyter-execute::

    import finesse_ligo
    from finesse_ligo.factory import aligo

    factory = aligo.ALIGOFactory("lho_O4.yaml")

The factory is provided with a YAML file that sets up the
`here <https://git.ligo.org/finesse/finesse-ligo/-/tree/main/src/finesse_ligo/parameter_files>`_
the initial parameters. We attempt to keep these YAML files up to date with our
best knowledge of a detector. Remember many of the YAML parameters here can
always be changed later after the factory has made a model.

To make a model we simply use

.. jupyter-execute::

    model = factory.make()
    print(model)

The making step may take a few seconds. Internally it is doing various
optimisations to try and find a good operating point for the model.

If you want to see everything you can print out `model.elements` and you'll see
that you have a lot of elements, some will look familiar, such as `ITMX` or the
`BS`. At this stage you have a |Finesse| model and can do anything you normally
would with a model such as run simulations, add new components, change parameter
values.

Next we'll dive into more details about what :ref:`the model contains
<the_model>` and how to use the :ref:`factory <the_factory>`. Of crucial
importance is correctly locking the model and :ref:`finding a good operating
point <_length_locking>.
