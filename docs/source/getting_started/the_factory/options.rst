.. include:: /defs.hrst
.. _factory_options:

ALIGO factory options
=====================

The parameters that are being used by the factory can be seem by checking the
:code:`factory.options` attribute. This is a `Munch
<https://github.com/Infinidat/munch>`_ object which is a special dictionary that
can have its entries accessed like attributes to save typing so many :code:`[]`.

Default options
---------------

.. jupyter-execute::

    import pprint
    from finesse_ligo.factory import aligo
    factory = aligo.ALIGOFactory("llo_O4.yaml")

    pprint.PrettyPrinter(indent=4, sort_dicts=True).pprint(
        factory.options.toDict()
    )


If you want to change options you can access them like

.. jupyter-execute::

    factory.options.INPUT.add_IMC_and_IM1 = True

If you want to reset back to defaults you can call :code:`factory.reset()`.

General
-------

General options are in the top level of the options.

* :code:`QUAD_suspension_model`: The class to use for the suspensions of the
  test masses. This could be a simple :external:class:`Pendulum
  <finesse.components.mechanical.Pendulum>`, :external:class:`FreeMass
  <finesse.components.mechanical.FreeMass>`, or full Quadruple suspension.
* :code:`QUAD_suspension_kwargs`: A dictionary of kwarg settings for whatever
  class is set as the suspenion model.
* :code:`add_118MHz`: Adds a 118MHz modulator at the input
* :code:`add_45MHz`: Adds a 45MHz modulator at the input
* :code:`add_9MHz`: Adds a 9MHz modulator at the input
* :code:`add_detectors`: When false no detectors will be added to the model and
  this will be left to the user to add
* :code:`add_transmon` : When True the transmon telescopes are added
* :code:`fake_src_gouy` : When True, a parameter :code:`SRC_gouy` is added to
  the model. This is used to set the Gouy accumulated along the space between
  SRM and SR2. Distancs from SR2 to BS will accumulate 0 degrees of Gouy phase.
  Note that this is not physical, the SRC eigen mode will not change! The
  default value is 20 degrees. :code:`SRC_gouy` will correspond to approximately
  a single pass Gouy phase for the SRC. :code:`SRC_gouy_astig` will add degrees
  to the horiztonal Gouy phase and subtract from the vertical Gouy phase.
* :code:`fake_prc_gouy` : When True, a parameter :code:`PRC_gouy` is added to
  the model. This is used to set the Gouy accumulated along the space between
  PRM and PR2. Distancs from PR2 to BS will accumulate 0 degrees of Gouy phase.
  Note that this is not physical, the PRC eigen mode will not change! The
  default value is 20 degrees. :code:`PRC_gouy` will correspond to approximately
  a single pass Gouy phase for the PRC. :code:`PRC_gouy_astig` will add degrees
  to the horiztonal Gouy phase and subtract from the vertical Gouy phase.

thermal
-------
* :code:`add` : When True, thermal effects are added

INPUT
-----
* :code:`add_IMC_and_IM1` : When False, the IMC and IM1 (as it's flat) are not
  added to the model. Instead the IMC eigenmode is calculated from the YAML
  settings and propagated to the input of IM2.

materials
---------
* :code:`test_mass_substrate` : The material to use for the test mass substrates,
  see the options in :external:class:`finesse.materials.Material`.

ASC
---
* :code:`add` : Whether to add ASC features or not
* :code:`add_AC_loops` : Adds in the AC signal paths
* :code:`add_DOFs` : Adds in the ASC degrees of freedom, CHARD_Y, CHARD_P, etc
* :code:`add_locks` : Add the DC lock commands for ASC DOFs
* :code:`add_output_detectors` : Add in detectors to readout ASC motions
* :code:`add_readouts` : Add in DC and RF QPD readout elements
* :code:`close_AC_loops` : If AC loops are added close those loops

LSC
---
* :code:`add_AC_loops` : Add AC control loops for LSC
* :code:`close_AC_loops` : Whether to close the LSC loops
* :code:`add_DOFs` : Adds LSC length degrees of freedom, CARM, DARM, etc.
* :code:`add_locks` : Add in DC locks for length control
* :code:`add_output_detectors` : Add in output detectors for LSC error signals
* :code:`add_readouts` : Add in DC and RF photodiode readout elements

SQZ
---
* :code:`add` : When True, the filter cavity, squeezer, and adaptive optics used to
  inject the squeezed state into the interferometer are added.
* :code:`add_detectors` : When True, the :external:class:`QuantumNoiseDetector` needed
  to calculate quantum noise is added. This can be slow and is unnecessary for many
  squeezing studies.
* :code:`add_seed_laser` : When True, a diagnostic seed laser is added which injects
  light into the VOPO injection platform and can be used for mode scans etc.

Apertures
---------
* :code:`add` : Global switch for adding apertures or not
* :code:`BS_HR` : Beamspliter HR aperture
* :code:`BS_ITM` : Beamsplitter AR, CP, ESD apertures
* :code:`PR3_SR3` : PR3 and SR3 apertures
* :code:`test_mass` : Add apertures to HR surface of test masses
* :code:`use_surface_profile` : Add in the measured surface profiles of the test masses
* :code:`make_axisymmetric` : When True, the surface profiles are all made
  axi-symmetric to reduce odd order mode couplings, useful for quickly
  testing systems with symmetric effects, ie. even maxtem.
