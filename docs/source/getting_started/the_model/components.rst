.. include:: /defs.hrst
.. _components:

Components
==========


.. jupyter-execute::

    import finesse_ligo
    from finesse_ligo.factory import aligo

    factory = aligo.ALIGOFactory("lho_O4.yaml")
    model = factory.make()
