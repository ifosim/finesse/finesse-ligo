.. include:: /defs.hrst
.. _tracing_beams:

Propagating Gaussian beam parameters
====================================

For |Finesse| models to use higher order modes you must specify what gaussian
beam parameters to use, :math:`q=z+\I z_r`, where :math:`z` and :math:`z_{r}` are the relative
waist location and Rayleigh range of the beam. Every node in a model must have a
:math:`q`-parameter assigned to it. See
:external:ref:`higher_order_spatial_modes_manual` for more details on how |Finesse|
works with higher order modes.

This is laborious to do by hand, so |Finesse| has an ABCD tracing algorithm
included where you state which cavity eigenmodes should be traced throughout the
model. This is useful to propagate some beam shape from a cavity throughout the
system. Alternatively you can also use :external:meth:`propagate_beam() <finesse.model.Model.propagate_beam>` where given
some model you can propagate some input beam from one node to another in a
single pass manner.

As an example you can propagate a beam at the input from IM2.p1.i to PRM.p2.i using:

.. jupyter-execute::

    import finesse
    import finesse_ligo
    from finesse_ligo.factory import aligo

    finesse.init_plotting()

    factory = aligo.ALIGOFactory("lho_O4.yaml")
    model = factory.make()

    prop = model.propagate_beam('IM2.p1.i', 'PRM.p2.i', direction='x')
    prop.plot()

You can also print out the values in a table form:

.. jupyter-execute::

    prop.table()

Similarly you can interogate the propagation solution to find out more details:

.. jupyter-execute::

    prop.q('PRM.p2.i')

The :external:class:`BeamParam <finesse.gaussian.BeamParam>` object has the usual accesibly parameters such as waist
size, position, spot size, curvature, etc. Please see the |Finesse|
documentation for more details.
