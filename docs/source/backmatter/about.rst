.. include:: /defs.hrst
.. _contributors:

About us
========

|Finesse| LIGO is a child package of the main |Finesse| tool. It contains LIGO
specific models and functionality that are regularly used to commission and
understand the detector.

Finesse-LIGO development team:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. only:: html

  .. cssclass:: avatar
  .. figure:: /images/daniel.jpg

.. cssclass:: avatar-text
**Daniel Brown**, University of Adelaide: project and programming lead


.. only:: html

  .. cssclass:: avatar
  .. figure:: /images/kevin.png

.. cssclass:: avatar-text
**Kevin Kuns**, MIT
