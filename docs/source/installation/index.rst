.. include:: ../defs.hrst
.. _installation:

Installation
============

For those completely new to |Finesse| LIGO you must first ensure you install the
core package. More details can be found
here`<https://finesse.ifosim.org/docs/latest/getting_started/install/index.html>_`

Once |Finesse| is installed you can install this LIGO package in one of two
ways. For those looking to use the standard currently available package you can
install it via Pypi with

.. code-block:: console

    $ pip install finesse-ligo

The Pypi package though will often be behind what current scientists are using
to model LIGO currently. The main repository is at:

`<https://git.ligo.org/finesse/finesse-ligo>`_

There is also a gitlab.com mirror
`<https://gitlab.com/ifosim/finesse/finesse-ligo>`_ for those to fork and merge
request too who are outside the LSC.

For those looking to just use the latest version which may contain unreleased
fixes and do not want to make any changes to the code-base, you can git clone
this repository and install using

.. code-block:: console

    $ pip install git+https://git.ligo.org/finesse/finesse-ligo.git

If you have already installed the package and just want to update run

.. code-block:: console

    $ pip install --upgrade git+https://git.ligo.org/finesse/finesse-ligo.git

The installation will show you have installed a package with a version tag that
looks something like :code:`finesse-ligo-1.0.16.dev151+g165f3a1`. The `dev151`
describes how many git commits have been made since :code:`1.0.16` was released.
:code:`g165f3a1` is the git commit ID value that you are using specifically.

For those looking to make changes to the finesse-ligo package you will need to
fork the project on git.ligo.org and then make a merge request with your changes.

The "ddb" branch
----------------

The `ddb <https://git.ligo.org/finesse/finesse-ligo/-/tree/ddb?ref_type=heads>`_
branch of finesse-ligo is Daniel Brown's branch, typically the latest
developments are usually happening in there. Some of these may however be
experimental and likely to break, so only use it if you know what you're doing.
Ideally you should just be using the `main
<https://git.ligo.org/finesse/finesse-ligo>`_ branch.
