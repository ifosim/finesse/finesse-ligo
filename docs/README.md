# Finesse 3 documentation

These directories contain the reStuctured Text (reST) files (`.rst` extension) making up
the documentation for the `finesse` package. For information on how to build the
documentation, see the [existing
documentation](https://finesse.ifosim.org/docs/develop/developer/documenting.html#building-the-documentation).


Use `python -m sphinx.ext.intersphinx https://finesse.ifosim.org/docs/latest/objects.inv > finesse-lookup.txt`
to generate a lookup table for what can be referenced in the Finesse manual.
You can then make references using :external:class:`blah`,
:external:meth:`blah`, etc using the references.
