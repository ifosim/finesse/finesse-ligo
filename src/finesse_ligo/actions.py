import numpy as np
from finesse.analysis.actions import (
    Action,
    Maximize,
    Minimize,
    PseudoLockCavity,
    Series,
    Change,
    Noxaxis,
    OptimiseRFReadoutPhaseDC,
    SetLockGains,
    RunLocks,
    Execute,
)
from finesse.env import warn
import finesse_ligo.exceptions


class DARM_RF_to_DC(Action):
    """Locks a model using DARM RF readout then transitions the model into using a DC
    readout and locks.

    Parameters
    ----------
    dc_lock_shift : float, optional
        Size and direction which the action should shift the DARM offset before
        trying to lock. You can use this to change which side of DARM quadratic
        error signal to lock to.
    """

    def __init__(self, dc_lock_shift=0.5e-3, name="DarmRF2DC"):
        super().__init__(name)
        self.dc_lock_shift = dc_lock_shift
        self.__lock_rf = RunLocks(
            "DARM_rf_lock", max_iterations=1000, name="run RF lock"
        )
        self.__lock_dc = RunLocks(
            "DARM_dc_lock", max_iterations=1000, name="run DC lock"
        )

    def _do(self, state):
        self.__lock_rf._do(state)
        state.model.DARM_rf_lock.enabled = False
        # kick lock away from zero tuning for DC lock to grab with
        state.model.DARM.DC += self.dc_lock_shift
        # DARM quadratic is always going to have a ±dx/dy for a ±dx
        # We then use the opposite sign to make it negative feedback
        state.model.DARM_dc_lock.gain = -1 * np.sign(self.dc_lock_shift) * 0.01
        state.model.DARM_dc_lock.enabled = True
        self.__lock_dc._do(state)
        return None

    def _requests(self, model, memo, first=True):
        memo["changing_parameters"].append("DARM_dc_lock.gain")
        memo["changing_parameters"].append("DARM_dc_lock.enabled")
        memo["changing_parameters"].append("DARM_rf_lock.enabled")
        self.__lock_rf._requests(model, memo)
        self.__lock_dc._requests(model, memo)
        return memo


class DRFPMI_state(Action):
    """Assumes a mode has a PRM, SRM, ITMX, ETMX, ITMY, and ETMY mirror elements in.
    This action will change the alignment state of these. The options are:

    'PRMI', 'SRMI', 'DRMI', 'MI', 'FPMI', 'PRFPMI', 'SRFPMI', 'DRFPMI', 'XARM', 'YARM'

    This action will change the state of the model.
    """

    states = (
        "PRMI",
        "SRMI",
        "DRMI",
        "MI",
        "FPMI",
        "PRFPMI",
        "SRFPMI",
        "DRFPMI",
        "XARM",
        "YARM",
    )

    def __init__(self, state: str, name="drfpmi_state"):
        super().__init__(name)

        if state not in self.states:
            raise ValueError(f"State '{state}' is not a valid option: {self.states}")
        self.state = state

    def _do(self, state):
        if self.state == "PRMI":
            state.model.PRM.misaligned = 0
            state.model.SRM.misaligned = 1
            state.model.ETMX.misaligned = 1
            state.model.ITMX.misaligned = 0
            state.model.ETMY.misaligned = 1
            state.model.ITMY.misaligned = 0
        elif self.state == "SRMI":
            state.model.PRM.misaligned = 1
            state.model.SRM.misaligned = 0
            state.model.ETMX.misaligned = 1
            state.model.ITMX.misaligned = 0
            state.model.ETMY.misaligned = 1
            state.model.ITMY.misaligned = 0
        elif self.state == "DRMI":
            state.model.PRM.misaligned = 0
            state.model.SRM.misaligned = 0
            state.model.ETMX.misaligned = 1
            state.model.ITMX.misaligned = 0
            state.model.ETMY.misaligned = 1
            state.model.ITMY.misaligned = 0
        elif self.state == "MI":
            state.model.PRM.misaligned = 1
            state.model.SRM.misaligned = 1
            state.model.ETMX.misaligned = 1
            state.model.ITMX.misaligned = 0
            state.model.ETMY.misaligned = 1
            state.model.ITMY.misaligned = 0
        elif self.state == "FPMI":
            state.model.PRM.misaligned = 1
            state.model.SRM.misaligned = 1
            state.model.ETMX.misaligned = 0
            state.model.ITMX.misaligned = 0
            state.model.ETMY.misaligned = 0
            state.model.ITMY.misaligned = 0
        elif self.state == "PRFPMI":
            state.model.PRM.misaligned = 0
            state.model.SRM.misaligned = 1
            state.model.ETMX.misaligned = 0
            state.model.ITMX.misaligned = 0
            state.model.ETMY.misaligned = 0
            state.model.ITMY.misaligned = 0
        elif self.state == "SRFPMI":
            state.model.PRM.misaligned = 1
            state.model.SRM.misaligned = 0
            state.model.ETMX.misaligned = 0
            state.model.ITMX.misaligned = 0
            state.model.ETMY.misaligned = 0
            state.model.ITMY.misaligned = 0
        elif self.state == "DRFPMI":
            state.model.PRM.misaligned = 0
            state.model.SRM.misaligned = 0
            state.model.ETMX.misaligned = 0
            state.model.ITMX.misaligned = 0
            state.model.ETMY.misaligned = 0
            state.model.ITMY.misaligned = 0
        elif self.state == "YARM":
            state.model.PRM.misaligned = 1
            state.model.SRM.misaligned = 1
            state.model.ETMX.misaligned = 1
            state.model.ITMX.misaligned = 1
            state.model.ETMY.misaligned = 0
            state.model.ITMY.misaligned = 0
        elif self.state == "XARM":
            state.model.PRM.misaligned = 1
            state.model.SRM.misaligned = 1
            state.model.ETMX.misaligned = 0
            state.model.ITMX.misaligned = 0
            state.model.ETMY.misaligned = 1
            state.model.ITMY.misaligned = 1
        else:
            raise Exception(f"{self.state} not implemented")

    def _requests(self, model, memo, first=True):
        # changing the mirror misaligned parameter is essentially
        # changing the mirror reflectivity model parameter
        memo["changing_parameters"].extend(
            (
                "PRM.misaligned",
                "SRM.misaligned",
                "ETMX.misaligned",
                "ITMX.misaligned",
                "ETMY.misaligned",
                "ITMY.misaligned",
            )
        )
        return memo


def InitialLockLIGO(
    LSC_demod_opt=(
        "CARM",
        "REFL9_I",
        "PRCL",
        "POP9_I",
        "SRCL",
        "POP45_I",
        "DARM",
        "AS45_Q",
    ),
    run_locks=True,
    exception_on_lock_fail=True,
    exception_on_check_fail=True,
    gain_scale=0.5,
    lock_steps=2000,
    pseudo_lock_arms=False,
):
    """Initial locking action, tries to somewhat replicate the locking prodcedure for
    LSC.

    If it can't find a good operating point then the RunLocks step at the end will fail.
    """

    def ALERT(x):
        if exception_on_check_fail:
            raise finesse_ligo.exceptions.InitialLockCheckException(x)
        else:
            warn(x)

    def check_arm_gains(state, name):
        X_GAIN = state.previous_solution["AGX"]
        Y_GAIN = state.previous_solution["AGY"]
        X_GAIN_APRX = 4 / state.model.ITMX.T
        Y_GAIN_APRX = 4 / state.model.ITMY.T

        if not (0.8 <= X_GAIN / X_GAIN_APRX <= 1.2):
            ALERT(
                f"X arm gain is significantly different to what was expected = {X_GAIN:.1f} vs {X_GAIN_APRX:.1f}"
            )
        if not (0.8 <= Y_GAIN / Y_GAIN_APRX <= 1.2):
            ALERT(
                f"Y arm gain is significantly different to what was expected = {Y_GAIN:.1f} vs {Y_GAIN_APRX:.1f}"
            )

    if pseudo_lock_arms:
        arm_locks = (
            PseudoLockCavity(
                "cavXARM", lowest_loss=False, feedback="XARM.DC", name="lock XARM"
            ),
            PseudoLockCavity(
                "cavYARM", lowest_loss=False, feedback="YARM.DC", name="lock YARM"
            ),
        )
    else:
        arm_locks = (
            Maximize("a_carrier_00_x", "XARM.DC", name="lock XARM"),
            Maximize("a_carrier_00_y", "YARM.DC", name="lock YARM"),
        )

    action = Series(
        Change(
            {
                "PRM.misaligned": True,
                "SRM.misaligned": True,
                "CARM.DC": 0,
                "DARM.DC": 0,
                "SRCL.DC": 0,
                "PRCL.DC": 0,
                "MICH.DC": 0,
                "MICH2.DC": 0,
            },
            name="misalign PRM,SRM",
        ),
        # Lock each arm cavity to the lowest loss mode
        *arm_locks,
        # Put mich on dark fringe
        Minimize("Pas_carrier", "MICH2.DC", name="find dark AS"),
        Noxaxis(name="after ARMs+AS"),
        Execute(check_arm_gains),
        # Realign the PRM
        Change({"PRM.misaligned": False}, name="align PRM"),
        # get the PRC in roughly the right place whilst keeping arms on resonance
        Maximize("PRG", "PRCL.DC", name="maximise PRG"),
        # get the PRC in roughly the right place whilst keeping arms on resonance
        Maximize("cost_prcl", ["PRCL.DC", "CARM.DC"], name="maxmize Parm*PRG9"),
        Noxaxis(name="after PRC"),
        Execute(check_arm_gains),
        # Realign SRM
        Change({"SRM.misaligned": False}, name="align SRM"),
        # Try and find signal recycling condition
        Minimize("cost_srcl", "SRCL.DC", name="maximize SRC power"),
        Change({"SRCL.DC": 90}, relative=True),  # add 90 to get to RSE
        Minimize("cost_srcl", "SRCL.DC", name="minimize PRG45"),
        Noxaxis(name="after SRC"),
        Execute(check_arm_gains),
        OptimiseRFReadoutPhaseDC(*LSC_demod_opt),
        SetLockGains(d_dof_gain=1e-10, gain_scale=gain_scale),
        RunLocks(max_iterations=lock_steps, exception_on_fail=exception_on_lock_fail),
        Noxaxis(name="after locks"),
        Execute(check_arm_gains),
    )
    if not run_locks:
        action.actions = action.actions[:-3]
    return action
